import bottle
import json
import clockpunch.server.cpunch_db as cdb
import subprocess as sp
import os
import signal
import sys

def signal_handler(signal, frame):
    print('shutting down mongod...')
    mongo_proc.send_signal(15)
    print('shutting down cpunch server...')
    sys.exit(0)

os.makedirs(os.path.expanduser('~/.clockpunch/db'), exist_ok=True)
mongo_args = ['mongod', '--logpath', os.path.expanduser('~/.clockpunch/mongolog'), '--bind_ip',
              '127.0.0.1', '--port', '29292', '--dbpath', os.path.expanduser('~/.clockpunch/db')]
mongo_proc = sp.Popen(mongo_args)
signal.signal(signal.SIGINT, signal_handler)

dbuser = cdb.DBUser(port=29292)         # create a new dbuser instance to start handling the data package

@bottle.route('/clockpunch', method='POST')
def handle_request():
    raw_return = bottle.request.body.readline()
    return_pack = json.loads(raw_return.decode('utf-8'))

    send_pack = dbuser.handle_dict(return_pack)

    raw_send = json.dumps(send_pack).encode('utf-8')

    return raw_send


print('Ctrl-C to gracefully shut down server')
bottle.run(host='0.0.0.0', port=60606)
