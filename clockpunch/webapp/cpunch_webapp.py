#!/bin/env python
# Ryan Doyle
#

from flask import app, render_template, Flask, request, make_response
from flask_wtf import Form
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired
import hashlib
from clockpunch.cpunch import Client


class MyForm(Form):
    name = StringField(validators=[DataRequired()])
    server = StringField('server: ', validators=[DataRequired()])
    port = StringField('port: ', validators=[DataRequired()])
    passw = PasswordField('password: ')


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def handle():
    if request.method == 'POST':

        # === DEFINE FUNCTION ===
        newpass = None
        if request.form['submit'] == 'Punch In':
            cp_func = 'i'
        elif request.form['submit'] == 'Punch Out':
            cp_func = 'o'
        elif request.form['submit'] == 'My Timecard':
            cp_func = 'r'
        elif request.form['submit'] == 'Register User':
            cp_func = 'reg'
        elif request.form['submit'] == 'Change Password':
            if not request.form.get('g-recaptcha-response', False):
                return htm_open + 'You did not complete the captcha!' + htm_close
            cp_func = 'change'
            newpass = request.form['newpassw']
        elif request.form['submit'] == 'List Management':
            cp_func = 'lm'
        elif request.form['submit'] == 'Add Manager':
            cp_func = 'am_{0}'.format(request.form['manager'])
        elif request.form['submit'] == 'Remove Manager':
            cp_func = 'rm_{0}'.format(request.form['manager'])
        elif request.form['submit'] == 'View Their Timecard':
            cp_func = 'ls_{0}'.format(request.form['sub'])
        else:
            return htm_open + 'something went wrong :(' + htm_close

        # === DEFINE NAME ===
        if request.form['name'] == "":
            return htm_open + 'You need a username at the very least!' + htm_close
        else:
            cp_user = request.form['name']

        cp_server = 'clockpun.ch'
        cp_port = 60606

        # === DEFINE PASS ===
        if request.form['passw'] == "":
            cp_hashed_pass = hashlib.sha512(cp_user.encode('utf-8')).hexdigest()
        else:
            cp_hashed_pass = hashlib.sha512((request.form['passw']).encode('utf-8')).hexdigest()

        if newpass:
            if newpass == "":
                newpass = hashlib.sha512(cp_user.encode('utf-8')).hexdigest()
            else:
                newpass = hashlib.sha512(newpass.encode('utf-8')).hexdigest()

        try:
            cp_client = Client(cp_server, cp_port)
        except Exception as e:
            return htm_open + "failed to connect to server :(" + htm_close

        # GET THA MAGIC GOIN

        data_pack = cp_client.send_payload(cp_user, cp_func, cp_hashed_pass, newpass=newpass)

        return Client.print_to_browser(data_pack)

    else:
        form = MyForm(csrf_enabled=False)
        resp = make_response(render_template("form.html", form=form))
        return resp

@app.route('/pwchange.html', methods=['GET', 'POST'])
def changepw():
    form = MyForm(csrf_enabled=False)
    resp = make_response(render_template("pwchange.html", form=form))
    return resp

@app.route('/management.html', methods=['GET', 'POST'])
def management():
    form = MyForm(csrf_enabled=False)
    resp = make_response(render_template("management.html", form=form))
    return resp

@app.route('/punches.html', methods=['GET', 'POST'])
def punches():
    form = MyForm(csrf_enabled=False)
    resp = make_response(render_template("punches.html", form=form))
    return resp



htm_open = '<html><body bgcolor="#2b2b2b"><center><i><font color="gray">clockpunch</font></i><br><br><font color="white"><b>'
htm_close = '</b></font></center></body></html>'


app.run(host='0.0.0.0', port=80)
