__author__ = 'rcdoyle'

import pymongo
import time
import re

class DBUser:

    def __init__(self, port=29292):
        self.client = pymongo.MongoClient('localhost', port)
        self.authenticated = False

    def handle_dict(self, obj):
        if obj['func'] == 'reg':
            return self._register_user(obj)

        if not self._check_user(obj['user']):
            return {'message': 'user does not exist!'}
        if not self._check_pass(obj['user'], obj['hash']):
            return {'message': 'could not authenticate :('}

        if obj['func'] == 'i' or obj['func'] == 'o':
            return self._punch(obj)
        if obj['func'] == 'change':
            return self._change_password(obj)
        if obj['func'] == 'r':
            return self._get_values(obj)  # should be a list.  find out size of list in server and format response there

        if obj['func'] == 'lm':
            return self._list_managers(obj)
        if obj['func'].startswith('ls_'):
            return self._list_subordinates(obj)
        if obj['func'].startswith('rm_'):
            return self._revoke_manager(obj)
        if obj['func'].startswith('am_'):
            return self._add_manager(obj)

    # checking that the password is correct for the user
    def _check_pass(self, username, hashed_pass):
        db = self.client[username]
        infocol = db['info']
        if infocol.find_one()['hash'] == hashed_pass:
            return True
        else:
            return False

    def _check_user(self, username):
        if username in self.client.database_names():
            return True
        return False

    def _clean_punches(self, username):
        three_weeks_ago = time.time() - 1814400
        db = self.client[username]

        punchcol = db['punches']

        punchcol.delete_many({'time': {'$lt': three_weeks_ago}})

    def _register_user(self, data):

        if self._check_user(data['user']) is True:
            return {'message': 'username: ' + data['user'] + ' already exists!'}

        db = self.client[data['user']]
        infocol = db['info']

        infocol.insert_one(data)
        return {'message': 'successfully registered user ' + data['user']}

    def _change_password(self, data):
        db = self.client[data['user']]
        infocol = db['info']
        infocol.replace_one({'hash': {'$regex': '.*'}}, {'hash': data['newpass']})
        return {'message': 'Password Changed :)'}

    def _punch(self, obj):

        self._clean_punches(obj['user'])

        db = self.client[obj['user']]
        pincol = db['punches']

        # format document and insert
        pincol.insert_one({'type': obj['func'], 'time': obj['time']})

        return {'message': 'Punch Recorded :)'}

    def _get_values(self, obj):

        self._clean_punches(obj['user'])

        user_db = self.client[obj['user']]
        pincol = user_db['punches']

        three_weeks_ago = obj['time'] - 1814400

        pin_list = []
        pin_curs = pincol.find({'time': {'$gt': three_weeks_ago}})

        for i in pin_curs:
            i.pop('_id', None)
            pin_list.append(i)

        return pin_list

    def _add_manager(self, obj):

        newmanager = re.search('am_(.*)', obj['func']).groups(0)[0]

        if not self._check_user(newmanager):
            return {'message': 'proposed manager does not exist!'}

        user_db = self.client[obj['user']]
        manager_db = self.client[newmanager]
        managers_col_of_subordinates = manager_db['info']
        users_col_of_managers = user_db['info']

        users_col_of_managers.insert_one({'manager': newmanager})
        managers_col_of_subordinates.insert_one({'sub': obj['user']})

        return {'message': 'Manager added successfully! :)'}

    def _revoke_manager(self, obj):

        newmanager = re.search('rm_(.*)', obj['func']).groups(0)[0]

        if not self._check_user(newmanager):
            return {'message': 'proposed manager does not exist!'}

        user_db = self.client[obj['user']]
        manager_db = self.client[newmanager]
        managers_col_of_subordinates = manager_db['info']
        users_col_of_managers = user_db['info']

        users_col_of_managers.delete_one({'manager': newmanager})
        managers_col_of_subordinates.delete_one({'sub': obj['user']})

        return {'message': 'Manager removed successfully! :)'}

    def _list_managers(self, obj):
        cursor = self.client[obj['user']]['info'].find({'$or': [{'sub': {'$regex': '.*'}}, {'manager': {'$regex': '.*'}}]})
        retlist = []
        for x in cursor:
            x.pop('_id', None)
            retlist.append(x)
        return retlist

    def _list_subordinates(self, obj):
        cursor = self.client[obj['user']]['info'].find({'sub': {'$regex': '.*'}})
        sublist = []
        for x in cursor:
            sublist.append(x['sub'])

        subordinate = re.search('ls_(.*)', obj['func']).groups(0)[0]

        if subordinate not in sublist:
            return {'message': "You don't have permission to look at this user :("}

        user_db = self.client[subordinate]
        pincol = user_db['punches']

        three_weeks_ago = obj['time'] - 1814400

        pin_list = []
        pin_curs = pincol.find({'time': {'$gt': three_weeks_ago}})

        for i in pin_curs:
            i.pop('_id', None)
            pin_list.append(i)

        return pin_list
