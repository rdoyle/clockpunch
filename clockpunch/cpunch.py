#!/bin/env python

import json
import argparse
import hashlib
import getpass
import time
from http import client as htcli
from colors import bold

# Define our Client class with all lib fn
class Client:
    conn = None  # our main connection

    def __init__(self, server, port):
        try:
            self.conn = htcli.HTTPConnection(server, port=port)
        except Exception as e:
            print("Connection timed out!")

    def kill_conn(self):
        self.conn.close()

    def send_payload(self, user, func, hashed_pass, newpass=None):
        send_pack = {'user': user, 'func': func, 'hash': hashed_pass, 'time': time.time()}
        if newpass:
            send_pack['newpass'] = newpass
        raw_send = json.dumps(send_pack).encode('utf-8')

        if self.conn is None or (user, func, hashed_pass) is None:
            raise Exception("bad input or no connection present")

        # fire the request
        self.conn.request('POST', '/clockpunch', body=raw_send)

        # server does magic

        # handle the return and perform appropriate action depending on return
        raw_return = self.conn.getresponse().read()
        return_pack = json.loads(raw_return.decode('utf-8'))
        return return_pack

    @staticmethod
    def print_to_browser(data):

        # easy formatted printer for the returned data list
        # data should be in the form of a list of dicts
        # containing keys for 'type' (in or out) and 'time' (actual punch time).
        # The goal here is to format them and make it look pretty

        # time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(1347517370))

        htm_open = '<html><body bgcolor="#2b2b2b"><center><i><font color="gray">clockpunch</font></i><br><br>'
        htm_close = '</center></body></html>'
        rb_open = '<font color="pink"><b>'
        gb_open = '<font color="lightgreen"><b>'
        bb_open = '<font color="lightblue"><b>'
        wb_open = '<font color="white"><b>'
        w_open = '<font color="white">'
        w_close = '</font>'
        b_close = '</b></font>'

        rv = htm_open

        if 'message' in data:
            rv += (wb_open + data['message'] + b_close)
            rv += htm_close
            return rv

        assert isinstance(data, list)
        rv += (wb_open + '- - - - - - - - - - - - - - - - - - - - - - - - - - -' + b_close + '<br><br>')

        last_seconds = None
        last_pin = None
        emitted = True
        prefix = None

        havemanager=False
        havesubs=False

        # manager headers
        for i in data:
            if 'manager' in i:
                havemanager = True
            if 'sub' in i:
                havesubs = True
        if havemanager:
            rv += wb_open + 'Managers' + "<br>" + '-----------' + b_close + '<br>'
            for i in data:
                if 'manager' in i:
                    rv += gb_open + i['manager'] + b_close + '<br>'
            rv += '<br><br>'
        if havesubs:
            rv += wb_open + 'Subordinates' + "<br>" + '-----------' + b_close + '<br>'
            for i in data:
                if 'sub' in i:
                    rv += bb_open + i['sub'] + b_close + '<br>'
            rv += '<br><br>'
            
        if havesubs or havemanager:
            rv += htm_close
            return rv

        # punch data
        for i in data:
            time_obj = time.localtime(i['time'])

            if i['type'] is 'o':
                if emitted is False:
                    seconds = i['time'] - last_seconds  # should give diff in seconds.

                    hours = seconds // (60 * 60)
                    seconds %= (60 * 60)
                    minutes = seconds // 60
                    seconds %= 60

                    if minutes < 10:
                        diff = "{0:.0f}:0{1:.0f}".format(hours, minutes)
                    else:
                        diff = "{0:.0f}:{1:.0f}".format(hours, minutes)

                    if hours >= 10:

                        rv += (wb_open + prefix + b_close + gb_open + (time.strftime('%I:%M %p --- ', last_pin)) + b_close +
                               wb_open + diff + b_close + bb_open + (time.strftime(' -> %I:%M %p', time_obj)) + b_close + '<br>')

                    else:
                        rv += (wb_open + prefix + b_close + gb_open + (time.strftime('%I:%M %p --- ', last_pin)) + b_close +
                               wb_open + diff + b_close + bb_open + (time.strftime(' --> %I:%M %p', time_obj)) + b_close + '<br>')

                    emitted = True

                elif emitted is True:
                    rv += (rb_open + prefix + b_close + bb_open + (time.strftime('%I:%M %p', last_pin)) + b_close + '&nbsp;&nbsp;' + w_open + '(out)' + w_close + '<br>')

            if i['type'] is 'i':
                if emitted is False:
                    rv += (rb_open + prefix + b_close + gb_open + (time.strftime('%I:%M %p', last_pin)) + b_close + '&nbsp;&nbsp;' + w_open + '(in)' + w_close + '<br>')
                    prefix = time.strftime('%b %d: ', time_obj)
                    last_pin = time_obj
                    last_seconds = i['time']

                elif emitted is True:
                    # just store to prev.
                    prefix = time.strftime('%b %d: ', time_obj)
                    last_pin = time_obj
                    last_seconds = i['time']
                    emitted = False

        if emitted is False:
            rv += (rb_open + prefix + b_close + gb_open + (time.strftime('%I:%M %p', last_pin)) + b_close + '&nbsp;&nbsp;' + w_open + '...' + w_close + '<br>')

        rv += htm_close

        return rv

    @staticmethod
    def print_to_term(data):

        # easy formatted printer for the returned data list
        # data should be in the form of a list of dicts
        # containing keys for 'type' (in or out) and 'time' (actual punch time).
        # The goal here is to format them and make it look pretty

        # time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(1347517370))

        if 'message' in data:
            print(data['message'])
            return

        assert isinstance(data, list)
        # print(bold('==== Punches in the past 3 weeks ====', fg='red'))
        print('- - - - - - - - - - - - - - - - - - -')

        last_seconds = None
        last_pin = None
        emitted = True
        prefix = None

        for i in data:
            if 'manager' in i:
                print(bold(i['manager'], fg='green'))
                continue
            if 'sub' in i:
                print(bold(i['sub'], fg='green'))
                continue

            time_obj = time.localtime(i['time'])

            if i['type'] is 'o':
                if emitted is False:
                    seconds = i['time'] - last_seconds  # should give diff in seconds.

                    hours = seconds // (60 * 60)
                    seconds %= (60 * 60)
                    minutes = seconds // 60
                    seconds %= 60

                    if minutes < 10:
                        diff = "{0:.0f}:0{1:.0f}".format(hours, minutes)
                    else:
                        diff = "{0:.0f}:{1:.0f}".format(hours, minutes)

                    if hours >= 10:
                        print(bold(prefix, fg='white') + bold(time.strftime('%I:%M %p --- ', last_pin), fg='green') +
                              bold(diff, fg='white') + bold(time.strftime(' -> %I:%M %p', time_obj), fg='blue'))

                    else:
                        print(bold(prefix, fg='white') + bold(time.strftime('%I:%M %p --- ', last_pin), fg='green') +
                              bold(diff, fg='white') + bold(time.strftime(' --> %I:%M %p', time_obj), fg='blue'))

                    emitted = True

                elif emitted is True:
                    print(bold(prefix, fg='red') + bold(time.strftime('%I:%M %p', last_pin), fg='blue'))

            if i['type'] is 'i':
                if emitted is False:
                    print(bold(prefix, fg='red') + bold(time.strftime('%I:%M %p', last_pin), fg='green'))
                    prefix = time.strftime('%b %d: ', time_obj)
                    last_pin = time_obj
                    last_seconds = i['time']

                elif emitted is True:
                    # just store to prev.
                    prefix = time.strftime('%b %d: ', time_obj)
                    last_pin = time_obj
                    last_seconds = i['time']
                    emitted = False

        if emitted is False:
            print(bold(prefix, fg='red') + bold(time.strftime('%I:%M %p', last_pin), fg='green'))

        return


def cli():
    parser = argparse.ArgumentParser(description='manage the username, key, and action args')

    parser.add_argument('-r', '--register', action='store_true', help='register: '
                                                                      'register with server. usage: '
                                                                      'cpunch.py -r -s clockpun.ch -u doylezdead')

    parser.add_argument('-am', '--addmanager', default=None, help='Authorize a manager to access your timecard')

    parser.add_argument('-n', '--nopassword', action='store_true', default=0, help='nopassword: '
                                                                                   'if used, the password '
                                                                                   'will be assumed blank')

    parser.add_argument('-s', '--server', default="clockpun.ch", help='server: '
                                                                      'The address of the server that is '
                                                                      'hosting the cpunch_server instance. '
                                                                      'default is clockpun.ch')

    parser.add_argument('-u', '--user', default=(getpass.getuser()), help='user: '
                                                                          'the username of the cpunch account. '
                                                                          'default is system username')

    parser.add_argument('-f', '--function', default='r', help='function: '
                                                              'the function to perform after auth. '
                                                              'i=in, o=out, lm=list managers, ls=list subordinates, r=retrieve. default is retrieve. '
                                                              'usage: '
                                                              'cpunch.py -u doylezdead -f i')

    parser.add_argument('-p', '--port', default=60606, help='port: '
                                                            'the port configured on the '
                                                            'clockpunch server.')

    args = parser.parse_args()

    cp_user = args.user
    cp_am = args.addmanager
    cp_func = args.function
    cp_reg = args.register
    cp_serv = args.server
    cp_port = args.port
    cp_np = args.nopassword

    # set connection here
    client_conn = Client(cp_serv, cp_port)

    if cp_reg:
        cp_func = 'reg'

        if cp_np:
            cp_hashed_pass = hashlib.sha512(cp_user.encode('utf-8')).hexdigest()
        else:
            cp_hashed_pass = hashlib.sha512(getpass.getpass('Please choose a password. '
                                                            'It will never be stored in'
                                                            'plain text: ').encode('utf-8')).hexdigest()

    else:
        if cp_am:
            cp_func = 'am_{0}'.format(cp_am)

        elif cp_func is None:
            print('No function specified, defaulting to retrieve punches')
            cp_func = 'r'

        if cp_np:
            cp_hashed_pass = hashlib.sha512(cp_user.encode('utf-8')).hexdigest()
        else:
            cp_hashed_pass = hashlib.sha512(
                getpass.getpass('Please enter your password for user ' + cp_user + ': ').encode('utf-8')).hexdigest()

    try:
        return_pack = client_conn.send_payload(cp_user, cp_func, cp_hashed_pass)
        client_conn.print_to_term(return_pack)

    except Exception as e:
        print("fail")

    # close up resources
    client_conn.kill_conn()

if __name__ == '__main__':
    cli()
